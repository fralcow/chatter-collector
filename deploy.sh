#!/bin/sh

ssh -o StrictHostKeyChecking=no $DIGITAL_OCEAN_USER@$DIGITAL_OCEAN_IP_ADDRESS << 'ENDSSH'
  cd ~/krup.tech/chatter-collector
  mv docker-compose.prod.yml docker-compose.yml
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull $IMAGE:web
  docker pull $IMAGE:postgres
  docker pull $IMAGE:nginx
  docker-compose up -d && docker-compose exec -T web python manage.py migrate && docker-compose exec -T web python manage.py collectstatic --no-input --clear
ENDSSH
