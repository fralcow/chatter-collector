Django website that fetches messages from telegram channels and shows the most viewed ones.

# Project structure

`./web/` -- django files
`./web/collect/` -- django app that collects telegram messages, sorts them and shows them as a webpage
`./web/tg_auth/` -- django app that manages authorization

# Setup

## Pipenv
1. `pipenv install` or any other virtual environment you prefer. Run in the `/web/` folder. Example for conda:
```
conda create --name chatter python==3.7
conda activate chatter
pip install django telethon asgiref asyncio
```

2. environmental variables.
   can be set in `.env` or manually via
```
export DJANGO_DEBUG = True
export TG_API_ID = 111111
export TG_API_HASH = '12345abcd'
```
3. pipenv shell -- start virtual environment
4. python manage.py migrate -- prepare the database
5. python manage.py runserver -- start the server
6. Go to `127.0.0.1:8000/` in your browser


## Docker with local config
1. `git clone git@gitlab.com:fralcow/chatter-collector.git`
2. Set necessary variables in .env
3. `mkcert foo.test` make certificates for local testing purposes and add them to `cert` folder. Make sure `foo.test` is in your local machine hosts file like following: `127.0.0.1	foo.test`
4. `docker-compose up --build`
5. `docker-compose exec web python3 manage.py migrate` to migrate the database

## Prod dependencies
rsync
