#!/bin/sh

echo DEBUG=0 >> .env
echo SQL_ENGINE=django.db.backends.postgresql >> .env

echo DJANGO_DEBUG=$DJANGO_DEBUG >> .env
echo TG_API_ID=$TG_API_ID >> .env
echo TG_API_HASH=$TG_API_HASH >> .env
echo SECRET_KEY=$SECRET_KEY >> .env
echo DJANGO_LOG_LEVEL=$DJANGO_LOG_LEVEL >> .env
echo POSTGRES_DB=$POSTGRES_DB >> .env
echo POSTGRES_USER=$POSTGRES_USER >> .env
echo POSTGRES_PASSWORD=$POSTGRES_PASSWORD >> .env
echo DB_SERVICE=$DB_SERVICE >> .env
echo DB_PORT=$DB_PORT >> .env
echo WEB_IMAGE=$IMAGE:web  >> .env
echo NGINX_IMAGE=$IMAGE:nginx  >> .env
echo POSTGRES_IMAGE=$IMAGE:postgres  >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env
