from django import forms
from django.core.validators import RegexValidator


class PhoneForm(forms.Form):
    phone_regex = RegexValidator(
            regex=r'^\+?\d{9,15}$',
            message="Enter you phone number in '+999999999' format. \
                     Up to 15 digits allowed.",
            )

    # validators should be a list
    phone = forms.CharField(
            validators=[phone_regex],
            max_length=17,
            required=True,
            initial="+",
            help_text="Please enter your phone in +999999999 format.",
            )


class CodeForm(forms.Form):
    number_regex = RegexValidator(
            regex=r'^[0-9]+$', message="Code can only contain numbers")

    code = forms.CharField(validators=[number_regex],
                           required=True)


class PasswordForm(forms.Form):
    password = forms.CharField(required=True, widget=forms.PasswordInput)
