import logging

from django.shortcuts import render, redirect
from django.views import View
from django.contrib import messages as django_messages
from django.urls import reverse

from telethon import errors

from .forms import PhoneForm, CodeForm, PasswordForm
from .helpers import (
        request_code, make_session_string, must_have_no_telegram_session,
        tg_logout)


logger = logging.getLogger("django-telethon-authorization")


def redirect_login(request):
    response = redirect('enter-phone')
    return response


def logout(request):
    session_string = request.session['session_string']
    tg_logout(session_string)
    request.session.flush()
    response = redirect('home')
    return response


class GetPhone(View):
    @must_have_no_telegram_session
    def get(self, request):
        form = PhoneForm()
        return render(request, 'phone.html', {'form': form})

    @must_have_no_telegram_session
    def post(self, request):
        form = PhoneForm(request.POST)

        if not form.is_valid():
            return render(request, 'phone.html', {'form': form})

        if form.is_valid():

            phone = form.cleaned_data['phone']
            request.session['phone'] = phone
            try:
                session_string, phone_hash = request_code(phone)
            except errors.PhoneNumberInvalidError:
                django_messages.add_message(
                        request, django_messages.ERROR,
                        "Phone number is incorrect",
                        )
                return render(request, 'phone.html', {'form': form})

            except errors.RPCError as e:
                z = e
                django_messages.add_message(
                        request, django_messages.ERROR,
                        z,
                        )
                return render(request, 'phone.html', {'form': form})

            request.session['session_string'] = session_string
            request.session['phone_hash'] = phone_hash
            request.session.set_expiry(600)
            return redirect('enter-code')


class EnterCode(View):
    @must_have_no_telegram_session
    def get(self, request):
        # user can only access enter code page after the phone is submitted
        if not request.session.get('phone'):
            url = reverse('enter-phone')
            django_messages.add_message(
                    request, django_messages.ERROR,
                    f"You need to <a href='{url}'>enter your phone\
                    </a> first.")
            return render(request, 'base.html')

        else:
            form = CodeForm()
            return render(request, 'code.html', {'form': form})

    @must_have_no_telegram_session
    def post(self, request):
        form = CodeForm(request.POST)
        if not form.is_valid():
            return render(request, 'code.html', {'form': form})

        if form.is_valid():

            code = form.cleaned_data['code']
            phone = request.session['phone']
            phone_hash = request.session['phone_hash']
            session_string = request.session['session_string']
            try:
                # pass session string, phone and code to do normal login and
                # get an exception if cloud password is needed
                make_session_string(
                        session_string=session_string,
                        phone=phone,
                        phone_hash=phone_hash,
                        code=code,)

            except errors.SessionPasswordNeededError:
                return redirect('enter-password')

            except errors.PhoneCodeInvalidError:
                django_messages.add_message(
                        request, django_messages.ERROR,
                        "Wrong code. Make sure you entered the correct code."
                        )
                return render(request, 'code.html', {'form': form})

        request.session['active_session'] = True
        return redirect('home')


class EnterPassword(View):
    @must_have_no_telegram_session
    def get(self, request):
        # user can only access enter code page after the phone is submitted
        if not request.session.get('phone'):
            django_messages.add_message(
                    request, django_messages.ERROR,
                    "You need to <a href='/auth/phone'>enter your phone\
                    </a> first.")
            return render(request, 'base.html')

        else:
            form = PasswordForm()
            return render(request, 'password.html', {'form': form})

    @must_have_no_telegram_session
    def post(self, request):
        form = PasswordForm(request.POST)
        if not form.is_valid():
            return render(request, 'password.html', {'form': form})

        if form.is_valid():
            password = form.cleaned_data['password']
            phone = request.session['phone']
            phone_hash = request.session['phone_hash']
            session_string = request.session['session_string']
            try:
                # pass session string, phone and password to do cloud password
                # login
                make_session_string(
                        session_string=session_string,
                        phone=phone,
                        phone_hash=phone_hash,
                        password=password,)

            except errors.PasswordHashInvalidError:
                django_messages.add_message(
                        request, django_messages.ERROR,
                        "Wrong cloud password. Make sure you use correct cloud \
                        password."
                        )
                return render(request, 'password.html', {'form': form})

        request.session['active_session'] = True
        return redirect('home')
