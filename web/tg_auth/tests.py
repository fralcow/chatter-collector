from django.test import TestCase
from django.urls import reverse
from .forms import PhoneForm, CodeForm
from unittest.mock import patch
from functools import wraps
from telethon import errors
from django.http import HttpRequest


class AuthorizationTests(TestCase):

    def mock_request_code(phone):
        if phone == "+123456789":
            return "session_string_instance", "phone_hash_instance"
        elif phone == "+123123123":
            request = HttpRequest()
            raise errors.PhoneNumberInvalidError(request)

    def mock_make_session_string(code=None, password=None, **kwargs):
        if code:
            if code == "123456":
                pass
            elif code == "123123":
                request = HttpRequest()
                raise errors.SessionPasswordNeededError(request)
            elif code == "121212":
                request = HttpRequest()
                raise errors.PhoneCodeInvalidError(request)
        if password:
            if password == "bad_password":
                request = HttpRequest()
                raise errors.PasswordHashInvalidError(request)
            elif password == "good_password":
                pass

    def clean_session(function):
        @wraps(function)
        def decorator(self, *args):

            client = self.client
            session = client.session
            keys = [*session.keys()]
            for key in keys:
                del session[key]
            session.save()

            return function(self, client, session, *args)

        return decorator

    # /auth/phone/ page
    @clean_session
    def test_phone_page_status_code(self, client, session):
        response = client.get(reverse('enter-phone'))
        self.assertEqual(response.status_code, 200)

    @clean_session
    def test_phone_page_view_url_by_name(self, client, session):
        response = client.get(reverse('enter-phone'))
        self.assertEqual(response.status_code, 200)

    @clean_session
    def test_phone_page_view_uses_correct_template(self, client, session):
        response = client.get(reverse('enter-phone'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'phone.html')

    @patch('tg_auth.views.request_code',
           side_effect=mock_request_code)
    @clean_session
    def test_good_phone_redirect(self, client, session, mock_request):
        response = client.post(reverse('enter-phone'),
                               {'phone': '+123456789'},
                               follow=True)
        self.assertEqual(client.session["session_string"],
                         "session_string_instance")
        self.assertEqual(client.session["phone_hash"],
                         "phone_hash_instance")
        self.assertRedirects(response, reverse('enter-code'))

    @patch('tg_auth.views.request_code',
           side_effect=mock_request_code)
    @clean_session
    def test_bad_phone_show_error(self, client, session, mock_request):
        # we need to pass a valid value to the field, but to imitate that
        # telegram can not find provided phone
        response = client.post(reverse('enter-phone'),
                               {'phone': '+123123123'})
        self.assertContains(
                response, 'Phone number is incorrect', html=True)

    # forms
    def test_phone_page_form_good(self):
        form_data = {'phone': '+123456789'}
        form = PhoneForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_phone_page_form_bad(self):
        form_data = {'phone': 'fitzgerald'}
        form = PhoneForm(data=form_data)
        self.assertFalse(form.is_valid())

    # /auth/code/ page
    @clean_session
    def test_code_status_code(self, client, session):
        response = client.get(reverse('enter-code'))
        self.assertEqual(response.status_code, 200)

    @clean_session
    def test_code_view_url_by_name(self, client, session):
        response = client.get(reverse('enter-code'))
        self.assertEqual(response.status_code, 200)

    @clean_session
    def test_code_view_uses_correct_template(self, client, session):
        ''' This test assumes page is opened after submitting a phone from the
        /auth/phone/ page
        '''
        session = client.session
        session["phone"] = 12345
        session.save()
        response = client.get(reverse('enter-code'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'code.html')

    @patch('tg_auth.views.make_session_string',
           side_effect=mock_make_session_string)
    @clean_session
    def test_good_code_redirect_no_2fa(
            self, client, session, mock_make_session_string):
        session = client.session
        session['phone'] = "+123456789"
        session['phone_hash'] = "phone_hash_instance"
        session['session_string'] = "session_string_instance"
        session.save()
        response = client.post(reverse('enter-code'),
                               {'code': "123456"},
                               )
        self.assertEqual(client.session["active_session"],
                         True)
        self.assertRedirects(
                response, reverse('home'), fetch_redirect_response=False)

    @patch('tg_auth.views.make_session_string',
           side_effect=mock_make_session_string)
    @clean_session
    def test_good_code_redirect_2fa(
            self, client, session, mock_make_session_string):
        session = client.session
        session['phone'] = "+123456789"
        session['phone_hash'] = "phone_hash_instance"
        session['session_string'] = "session_string_instance"
        session.save()
        response = client.post(reverse('enter-code'),
                               {'code': "123123"},
                               )
        self.assertRedirects(response,
                             reverse('enter-password'),
                             fetch_redirect_response=False)

    @patch('tg_auth.views.make_session_string',
           side_effect=mock_make_session_string)
    @clean_session
    def test_bad_code_show_error(
            self, client, session, mock_make_session_string):
        session = client.session
        session['phone'] = "+123456789"
        session['phone_hash'] = "phone_hash_instance"
        session['session_string'] = "session_string_instance"
        session.save()
        response = client.post(reverse('enter-code'),
                               {'code': "121212"},
                               )
        self.assertContains(
                response,
                "Wrong code. Make sure you entered the correct code.",
                html=True
                )

    # forms

    def test_code_form_good(self):
        form_data = {'code': '123456',
                     'password': 'fitzgerald'}
        form = CodeForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_code_form_good_no_pass(self):
        form_data = {'code': '123456'}
        form = CodeForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_code_form_bad(self):
        form_data = {'code': 'frank',
                     'password': 'fitzgerald'}
        form = CodeForm(data=form_data)
        self.assertFalse(form.is_valid())

    # /auth/password/ tests
    @clean_session
    def test_password_status_code(self, client, session):
        response = client.get(reverse('enter-password'))
        self.assertEqual(response.status_code, 200)

    @clean_session
    def test_password_get_by_name(self, client, session):
        response = client.get(reverse('enter-password'))
        self.assertEqual(response.status_code, 200)

    @clean_session
    def test_password_correct_template(self, client, session):
        session["phone"] = "+123456789"
        session.save()
        response = client.get(reverse('enter-password'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'password.html')

    @patch('tg_auth.views.make_session_string',
           side_effect=mock_make_session_string)
    @clean_session
    def test_password_correct_password_redirect(
            self, client, session, mock_make):
        session['password'] = "password_instance"
        session['phone'] = "+123456789"
        session['phone_hash'] = "abcd1234"
        session['session_string'] = "session_string_instance"
        session.save()
        response = client.post(reverse('enter-password'),
                               {'password': 'good_password'})
        self.assertEqual(client.session['active_session'], True)
        self.assertRedirects(
                response, reverse('home'), fetch_redirect_response=False)

    @patch('tg_auth.views.make_session_string',
           side_effect=mock_make_session_string)
    @clean_session
    def test_password_bad_incorrect_password_show_error(
            self, client, session, mock_make):
        session['password'] = "password_instance"
        session['phone'] = "+123456789"
        session['phone_hash'] = "abcd1234"
        session['session_string'] = "session_string_instance"
        session.save()
        response = client.post(reverse('enter-password'),
                               {'password': 'bad_password'})
        self.assertContains(
            response,
            "Wrong cloud password. Make sure you use correct cloud password.",
            html=True
            )
