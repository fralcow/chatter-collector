from django.urls import path, include
from .views import EnterCode, GetPhone, EnterPassword, redirect_login, logout


urlpatterns = [
        path('', redirect_login, name='login-start'),
        path('logout/', logout, name='logout'),
        path('phone/', GetPhone.as_view(), name='enter-phone'),
        path('code/', EnterCode.as_view(), name='enter-code'),
        path('password/', EnterPassword.as_view(), name='enter-password'),
]
