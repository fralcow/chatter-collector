import os
from asgiref.sync import async_to_sync
from django.contrib import messages as django_messages
from django.shortcuts import render
from functools import wraps

from telethon import TelegramClient
from telethon.sessions import StringSession


@async_to_sync
async def request_code(phone):

    api_id = os.environ["TG_API_ID"]
    api_hash = os.environ["TG_API_HASH"]
    client = TelegramClient(StringSession(), api_id, api_hash)
    await client.connect()

    # TODO: try catch for sending authorization code
    phone_hash = await client.send_code_request(phone)
    session_string = client.session.save()
    await client.disconnect()
    return (session_string, phone_hash.phone_code_hash)


@async_to_sync
async def tg_logout(session_string):
    api_id = os.environ["TG_API_ID"]
    api_hash = os.environ["TG_API_HASH"]
    client = TelegramClient(StringSession(session_string), api_id, api_hash)
    await client.connect()
    await client.log_out()


@async_to_sync
async def make_session_string(
        session_string,
        phone,
        phone_hash,
        code=None,
        password=None):
    """Submit the code or password to telegram to login for the first time

    :session_string: session string used to request code
    :phone: phone of the user
    :code: code received from the telegram
    :password: cloud password if one exists
    :returns: session_string of a logged in client

    """

    api_id = os.environ["TG_API_ID"]
    api_hash = os.environ["TG_API_HASH"]

    client = TelegramClient(StringSession(session_string),
                            api_id, api_hash)

    await client.connect()
    if code:
        await client.sign_in(phone=phone,
                             phone_code_hash=phone_hash,
                             code=code)

    elif password:
        await client.sign_in(phone=phone,
                             password=password)

    client.session.save()
    await client.disconnect()


def must_have_no_telegram_session(function):
    @wraps(function)
    def decorator(self, request):

        if request.session.get('active_session'):
            django_messages.add_message(
                    request, django_messages.ERROR,
                    "You are already logged in. Go to your <a href='/home'>\
                            homepage</a>.")
            return render(request, 'base.html')

        return function(self, request)

    return decorator
