import logging
import os
import os.path
from django.apps import AppConfig


class TelegramAuthConfig(AppConfig):
    name = 'tg_auth'

    def ready(self):

        logger = logging.getLogger("django-telethon-authorization")
        if not (
            os.environ.get("TG_API_ID", None) and
            os.environ.get("TG_API_HASH", None)
        ):
            logger.warning("TG_API_ID and AT_API_HASH are not set, falling back to dummy values!")
            os.environ["TG_API_ID"] = "123456"
            os.environ["TG_API_HAS"] = 'abcd1234'
