from django.shortcuts import render, redirect
from django.views import generic

from .helpers import (
        make_page_obj, make_forms_initials, make_form_messages,
        sort_form_messages, telegram_session_required)
from .forms import IntervalForm, ItemsPerPageForm, ItemSortingForm


def redirect_from_root(request):
    response = redirect('home/')
    return response


class ChannelsPage(generic.ListView):

    @telegram_session_required
    def get(self, request, *args, **kwargs):
        interval = request.session.get('interval', 1)
        session_string = request.session.get('session_string')
        form_messages = request.session.get('form_messages')
        items_per_page = request.session.get('items_per_page', 5)
        sorting = request.session.get('sorting', 'v')
        page_number = request.GET.get('page')

        context = {}
        if not form_messages:
            phone = request.session.get('phone')
            form_messages = make_form_messages(
                    phone=phone,
                    interval=interval,
                    session_string=session_string)
            request.session['form_messages'] = \
                sort_form_messages(form_messages)

            form_messages = request.session.get('form_messages')
            context['page_obj'] = make_page_obj(
                    form_messages, items_per_page, page_number)

        else:
            context['page_obj'] = make_page_obj(
                    form_messages, items_per_page, page_number)

        context['forms'] = make_forms_initials(
                interval, items_per_page, sorting)

        return render(request, 'home.html', context=context)

    @telegram_session_required
    def post(self, request, *args, **kwargs):
        form_interval = IntervalForm(request.POST)
        form_per_page = ItemsPerPageForm(request.POST)
        form_sorting = ItemSortingForm(request.POST)

        context = {}
        context['forms'] = {'interval_form': form_interval,
                            'per_page_form': form_per_page,
                            'sorting_form': form_sorting}

        if (
                not form_interval.is_valid() and
                not form_per_page.is_valid() and
                not form_sorting.is_valid()
           ):
            interval = request.session.get('interval', 1)
            items_per_page = request.session.get('items_per_page', 5)
            sorting = request.session.get('sorting', 'v')
            context['forms'] = \
                make_forms_initials(interval, items_per_page, sorting)
            if 'interval' in request.POST:
                context['forms']['period_form'] = form_interval
            if 'items_per_page' in request.POST:
                context['forms']['per_page'] = form_per_page
            if 'sorting' in request.POST:
                context['forms']['sorting'] = form_sorting
            return render(request, 'home.html', context=context)

        if form_interval.is_valid():
            request.session['interval'] = \
                    int(form_interval.cleaned_data['interval'])

            interval = request.session.get('interval', 1)
            phone = request.session.get('phone')
            session_string = request.session.get('session_string')
            sorting = request.session.get('sorting', 'v')

            form_messages = make_form_messages(
                    phone, session_string, interval)
            request.session['form_messages'] = \
                sort_form_messages(form_messages, sorting)
            form_messages = request.session.get('form_messages')
            items_per_page = request.session.get('items_per_page', 5)
            context['page_obj'] = make_page_obj(
                    form_messages, items_per_page)

            context['forms'] = make_forms_initials(
                    interval, items_per_page, sorting)

            return render(request, 'home.html', context=context)

        if (
            form_per_page.is_valid() or
            form_sorting.is_valid()
           ):
            if 'items_per_page' in request.POST:
                request.session['items_per_page'] = \
                    int(form_per_page.cleaned_data['items_per_page'])
            if 'sorting' in request.POST:
                request.session['sorting'] = \
                    form_sorting.cleaned_data['sorting']
                form_messages = request.session.get('form_messages')
                sorting = request.session.get('sorting', 'v')
                request.session['form_messages'] = \
                    sort_form_messages(form_messages, sorting)

            form_messages = request.session.get('form_messages')
            items_per_page = request.session.get('items_per_page', 5)
            context['page_obj'] = make_page_obj(
                    form_messages, items_per_page)

            interval = request.session.get('interval', 1)
            sorting = request.session.get('sorting', 'v')
            context['forms'] = make_forms_initials(
                    interval, items_per_page, sorting)

            return render(request, 'home.html', context=context)
