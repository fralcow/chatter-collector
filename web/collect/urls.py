from django.urls import path
from .views import ChannelsPage, redirect_from_root


urlpatterns = [
    path('', redirect_from_root, name='root'),
    path('home/', ChannelsPage.as_view(), name='home'),
]
