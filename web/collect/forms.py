from django import forms
from django.core.exceptions import ValidationError


class IntervalForm(forms.Form):
    interval = forms.ChoiceField(
            choices=[(x, x) for x in range(1, 8)],
            label="Interval, days",
            widget=forms.Select(
                attrs={
                    "onChange": "submit()",
                    "type": "button",
                    "class": "btn btn-primary dropdown-toggle",
                      }),)

    def clean_interval(self):
        data = int(self.cleaned_data['interval'])

        # Interval must be > 1
        if data < 1:
            raise ValidationError('Invalid interval - must be one day or more')

        return data


class ItemsPerPageForm(forms.Form):
    items_per_page = forms.ChoiceField(
            choices=[(5, 5), (10, 10), (25, 25), (50, 50)],
            widget=forms.Select(
                attrs={
                    "onChange": "submit()",
                    "type": "button",
                    "class": "btn btn-primary dropdown-toggle",
                      }),)

    def clean_items_per_page(self):
        data = int(self.cleaned_data['items_per_page'])

        if data < 1:
            raise ValidationError(
                    'Invalid items per page - must be more than one')

        return data


class ItemSortingForm(forms.Form):
    sorting = forms.ChoiceField(
            choices=[('v', 'Views'), ('p', 'Popularity')],
            widget=forms.Select(
                attrs={
                    "onChange": "submit()",
                    "type": "button",
                    "class": "btn btn-primary dropdown-toggle",
                      }),)

    def clean_item_sorting(self):
        data = str(self.cleanes_data['sorting'])
        if data not in ('v', 'p'):
            raise ValidationError(
                'Invalid sorting type - must be \'v\' or \'p\'')

        return data
