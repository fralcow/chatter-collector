from django.test import TestCase
from django.urls import reverse
from datetime import datetime
from unittest.mock import patch
from collect.forms import ItemsPerPageForm
from functools import wraps


class AuthorizationTests(TestCase):

    def normal_session_setup(function):
        @wraps(function)
        def decorator(self, *args):

            _client = self.client
            _session = _client.session
            _keys = [*_session.keys()]
            for key in _keys:
                del _session[key]
            _session['form_messages'] = \
                AuthorizationTests.make_dummy_messages()
            _session["active_session"] = True
            _session.save()

            return function(self, _client, _session, *args)

        return decorator

    def clean_session_setup(function):
        @wraps(function)
        def decorator(self):
            _client = self.client
            _session = _client.session
            _keys = [*_session.keys()]
            for key in _keys:
                del _session[key]
            _session.save()

            return function(self, _client, _session)

        return decorator

    def make_dummy_messages(amount=5):
        form_messages = []
        for x in range(1, amount+1):
            form_messages.append(
                    {
                        'title': f'title {x}',
                        'last_message': f'message {x}',
                        'views': x,
                        'popularity': amount+10-x,
                        'date': datetime.today().strftime("%Y-%m-%d"),
                        'message_url': f'url{x}',
                        }
                    )
        return form_messages

    def setUp(self):
        session = self.client.session
        session['form_messages'] = AuthorizationTests.make_dummy_messages()
        session["active_session"] = True
        session.save()

    @normal_session_setup
    def test_homepage_status_code(self, client, session):
        response = client.get(reverse('home'), )
        self.assertEqual(response.status_code, 200)

    @normal_session_setup
    def test_homepage_view_url_by_name(self, client, session):
        response = client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    @normal_session_setup
    def test_home_page_view_uses_correct_template(self, client, session):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    @patch('collect.views.make_form_messages',
           return_value=make_dummy_messages())
    @normal_session_setup
    def test_correct_post_order_on_first_load(
            self,
            client,
            session,
            mock_make_form_messages):
        # On first get_request of the homepage there are no form_messages in
        # session, we need to run make_form_messages to get messages from
        # telegram
        session["active_session"] = True
        session["form_messages"] = None
        session.save()
        response = client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            [x['views'] for x in response.context['page_obj'].object_list],
            sorted(
                [x['views'] for x in response.context['page_obj'].object_list],
                reverse=True)
            )

    @normal_session_setup
    def test_correct_post_order_views(self, client, session):
        response = client.post(reverse('home'), {'sorting': 'v'})
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            [x['views'] for x in response.context['page_obj'].object_list],
            sorted(
                [x['views'] for x in response.context['page_obj'].object_list],
                reverse=True)
        )

    @normal_session_setup
    def test_correct_post_order_popularity(self, client, session):
        response = client.post(reverse('home'), {'sorting': 'p'})
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            [x['popularity'] for x in
                response.context['page_obj'].object_list],
            sorted(
                [x['popularity'] for x in
                    response.context['page_obj'].object_list],
                reverse=True
                  )
            )

    @normal_session_setup
    def test_items_per_page(self, client, session):
        session["form_messages"] = AuthorizationTests.make_dummy_messages(50)
        session.save()
        form = ItemsPerPageForm()
        _choices = [x[0] for x in form.fields['items_per_page'].choices]
        for x in _choices:
            response = client.post(reverse('home'), {'items_per_page': x})
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.context['page_obj'].object_list), x)

    @normal_session_setup
    def test_5_items_per_page(self, client, session):
        session["form_messages"] = AuthorizationTests.make_dummy_messages(10)
        session.save()
        response = client.post(reverse('home'), {'items_per_page': 5})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['page_obj'].object_list), 5)

    @clean_session_setup
    def test_homepage_no_active_session(self, client, session):
        response = self.client.post(reverse('home'), {"sorting": "v"})
        url = reverse('login-start')
        self.assertEqual(response.status_code, 200)
        self.assertContains(
                response,
                f"You need to <a href='{url}'>log in</a>.", html=True
                )
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(
                response,
                f"You need to <a href='{url}'>log in</a>.", html=True
                )
