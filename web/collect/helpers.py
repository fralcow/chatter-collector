import os
from asgiref.sync import async_to_sync
from datetime import timedelta, date
from django.core.paginator import Paginator
from django.shortcuts import render
from django.contrib import messages as django_messages
from django.urls import reverse
from functools import wraps

from telethon import TelegramClient
from telethon.sessions import StringSession

from .forms import IntervalForm, ItemsPerPageForm, ItemSortingForm


@async_to_sync
async def get_chats(phone, session_string, interval=1):
    """
    Returns a tuple of ([messages], [channels])
    """
    api_id = os.environ["TG_API_ID"]
    api_hash = os.environ["TG_API_HASH"]
    client = TelegramClient(StringSession(session_string),
                            api_id, api_hash)
    await client.start()

    dialogs = client.iter_dialogs()
    messages, channels = [], []

    async for dialog in dialogs:
        # Since megagroups are also channels with a group flag, we need to
        # check that a channel is a channel and is not a group
        if dialog.is_channel and not dialog.is_group:
            _messages = await get_messages(dialog, client, interval)
            for message in _messages:
                messages.append(message)
                channels.append(dialog)

    await client.disconnect()
    return (messages, channels)


async def get_messages(dialog, client, interval):
    """Takes a dialog and a client to return all messages that are not older
    than interval days for that dialog"""

    messages = []
    async for message in client.iter_messages(dialog):
        if message.date.date() < (date.today() - timedelta(days=interval)):
            break

        # some messages have None views, we don't want that
        if message.views is not None:
            messages.append(message)

    return messages


def sort_by_views(form_messages):
    return form_messages.get('views')


def sort_by_popularity(form_messages):
    """Sorts the messages by popularity: message views/channel subscribers

    """
    return form_messages.get('popularity')


def make_page_obj(form_messages, items_per_page, page_number=1):

    paginator = Paginator(form_messages, items_per_page)
    page_obj = paginator.get_page(page_number)
    return page_obj


def make_forms_initials(interval, items_per_page, sorting):

    period_form = IntervalForm(
            initial={
                'interval': interval
                })
    per_page_form = ItemsPerPageForm(
            initial={
                'items_per_page': items_per_page,
                })
    sorting_form = ItemSortingForm(
            initial={
                'sorting': sorting,
                })

    return {'period_form': period_form,
            'per_page': per_page_form,
            'sorting': sorting_form}


def make_form_messages(phone, session_string, interval):

    messages_and_channels = get_chats(phone, session_string, interval)
    messages = messages_and_channels[0]
    channels = messages_and_channels[1]

    form_messages = [{
       'title': message.chat.title,
       'last_message': message.message,
       'views': message.views,
       'popularity': round(message.views/channel.entity.participants_count, 3),
       'date': message.date.strftime("%Y-%m-%d"),
       'message_url': make_message_url(message),
       }
       for message, channel in zip(messages, channels)]

    return form_messages


def sort_form_messages(form_messages, sorting='v'):
    """Sorts form messages
    """
    switcher = {
        'v': sort_by_views,
        'p': sort_by_popularity,
        }

    return sorted(form_messages, key=switcher.get(sorting), reverse=True)


def telegram_session_required(function):
    @wraps(function)
    def decorator(self, request):

        if not request.session.get('active_session'):
            url = reverse('login-start')
            django_messages.add_message(
                    request, django_messages.ERROR,
                    f"You need to <a href='{url}'>log in</a>.")
            return render(request, 'base.html')

        return function(self, request)

    return decorator


def make_message_url(message):
    """Creates a url for a message

    :message: Telethon.message type. The message should be posted in a channel.
    :returns: link of a format "https://t.me/denissexy/2477"

    """
    channel_name = message.chat.username
    message_id = message.id
    return f"https://t.me/{channel_name}/{message_id}"
